Instalation
----------

$ git clone 

$ composer update

Edit file .env

`DATABASE_URL=mysql://user:pass@127.0.0.1:3306/db`

$ php bin/console doctrine:schema:update -f

$ php bin/console doctrine:fixtures:load

$ php bin/console server:start

Testing
-------
$ ./vendor/bin/simple-phpunit

Run
---

Open browser:

* [http://localhost:8000/inheritance](http://localhost:8000/inheritance)
* [http://localhost:8000/array](http://localhost:8000/array)
* [http://localhost:8000/database](http://localhost:8000/database)
