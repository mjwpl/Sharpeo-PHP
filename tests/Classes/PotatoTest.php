<?php

namespace App\Tests\Classes;


use App\Classes\Plant;
use App\Classes\Potato;
use App\Classes\Vegetable;
use PHPUnit\Framework\TestCase;

class PotatoTest extends TestCase
{
    public function testPotatoIsVegetable()
    {
        $this->assertInstanceOf(Vegetable::class, new Potato);
    }

    public function testPotatIsPlant()
    {
        $this->assertInstanceOf(Plant::class, new Potato);
    }
}