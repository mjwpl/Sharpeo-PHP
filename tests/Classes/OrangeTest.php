<?php

namespace App\Tests\Classes;


use App\Classes\Orange;
use PHPUnit\Framework\TestCase;

class OrangeTest extends TestCase
{

    public function testTypeAttribute()
    {
        $this->assertClassHasAttribute('type', Orange::class);
    }

    public function testStaticMethodMake()
    {
        $this->assertContains('juice', Orange::make());
    }

}