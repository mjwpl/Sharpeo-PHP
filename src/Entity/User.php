<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $added;

//    /**
//     * @ORM\ManyToMany(targetEntity="Fruit", mappedBy="UsersFruits")
//     * @ORM\JoinTable(
//     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
//     *      inverseJoinColumns={@ORM\JoinColumn(name="fruit_id", referencedColumnName="id")}
//     *      )
//     */
//    private $fruit_id;

    public function __construct()
    {
        $this->added = new \DateTime();
        //$this->fruit_id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * @param \DateTime $added
     */
    public function setAdded(\DateTime $added): void
    {
        $this->added = $added;
    }

}