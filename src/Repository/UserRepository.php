<?php

namespace App\Repository;


use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class UserRepository
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Repository
 */
class UserRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUserWithFood()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery('SELECT u.name as username, f.name as fruit FROM App\Entity\User u 
                                   INNER JOIN App\Entity\UsersFruits uf WITH u.id = uf.user_id
                                   INNER JOIN App\Entity\Fruit f WITH f.id = uf.fruit_id
                                   WHERE f.name != :fruitname');
        $query->setParameter('fruitname', 'pomarańcza');

        return $query->getResult();
    }

}