<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArrayTaskController
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Controller
 */
class ArrayTaskController extends Controller
{

    /**
     * Funkcja tworzy tablicę użytkowników i owoców.
     * Przypisuje losowy owoc do użytkownika.
     * Sortuje alfabetycznie tablicę użytkownikó wg kluczy.
     * Usuwa wszystkich użytkowników posiadających 'jabłko'.
     * Wynik zwraca w postaci JSON
     *
     * @return JsonResponse
     * @throws \Exception @see http://php.net/manual/en/function.random-int.php
     *
     * @Route("/array", name="array_task")
     */
    public function arrayTaskAction()
    {
        $userArray  = ['Cezary' => null, 'Dariusz' => null, 'Bartosz' => null, 'Adam' => null];
        $fruitArray = ['kapusta' , 'ziemniak', 'jabłko', 'pomarańcza'];
        $response   = [];


        foreach ($userArray as &$user)
        {
            $user = $fruitArray[ random_int(0, count($fruitArray) - 1) ];
        }

        ksort($userArray);

        $response['users - before'] = $userArray;

        while($found = array_search('jabłko', $userArray, true))
        {
            unset($userArray[$found]);
        }

        $response['users - after'] = $userArray;

        return new JsonResponse($response);
    }

}