<?php

namespace App\Controller;


use App\Classes\Apple;
use App\Classes\Cabbage;
use App\Classes\Fruit;
use App\Classes\Orange;
use App\Classes\Potato;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InheritanceTaskController
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Controller
 */
class InheritanceTaskController extends Controller
{

    /**
     * Funkcja tworzy tablicę obiektów klas Cabbage, Potato, Apple
     * Dodaje do odpowiedzi zmienną klasową $type z każdego obiekty
     * Wywołuje prywatną metodę checkObject()
     * Wywołuje statyczną metodę make() z klasy Orange
     * Zwrca wynik w postaci JSON
     *
     * @return JsonResponse
     *
     * @Route("/inheritance", name="inheritance_task")
     */
    public function indexAction()
    {
        $response = [];
        $food     = [];

        $food[] = new Cabbage();
        $food[] = new Potato();
        $food[] = new Apple();

        foreach ($food as $item)
        {
            $response['getType'][] = $item->getType();

            try
            {
                $response['checkObject'][] = $this->checkObject($item);
            }
            catch(\TypeError $error)
            {
                $response['checkObject'][] = get_class($item) . ' is not a fruit';
            }
        }

        $response['Orange static method'][] = Orange::make();

        return new JsonResponse($response);
    }

    /**
     * @todo Zmienić typ parametru $obj na PlantInterface
     * @param Fruit $obj
     * @return string
     */
    private function checkObject (Fruit $obj)
    {
        return $obj->getType();
    }

}