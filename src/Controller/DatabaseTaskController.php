<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DatabaseTaskController
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Controller
 */
class DatabaseTaskController extends Controller
{

    /**
     * @Route("/database", name="database_task")
     */
    public function databaseAction(UserRepository $userRepository)
    {
        return new JsonResponse($userRepository->getUserWithFood());
    }

}