<?php

namespace App\DataFixtures;

use App\Entity\Fruit;
use App\Entity\User;
use App\Entity\UsersFruits;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AppFixtures
 *
 * Pomocnicza klasa inicjalizująca bazę danych.
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 *
 * @example $ php bin/console doctrine:fixtures:load
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception @see http://php.net/manual/en/function.random-int.php
     */
    public function load(ObjectManager $manager)
    {
        $userArray  = ['Adam', 'Bartosz', 'Cezary', 'Dariusz'];
        $fruitArray = ['kapusta' , 'ziemniak', 'jabłko', 'pomarańcza'];

        $userCnt    = count($userArray) ;

        for($i = 0; $i < $userCnt; $i++)
        {
            $user = new User();
            $user->setName($userArray[$i]);

            $fruit = new Fruit();
            $fruit->setName($fruitArray[$i]);

            $manager->persist($user);
            $manager->persist($fruit);
        }

        $manager->flush();

        $users  = $manager->getRepository('App:User')->findAll();
        $fruits = $manager->getRepository('App:Fruit')->findAll();

        $fruitsCnt = count($fruits);

        foreach ($users as $user)
        {
            $uf = new UsersFruits();
            $uf->setUserId($user->getId());
            $uf->setFruitId($fruits[random_int(0, $fruitsCnt - 1)]->getId());

            $manager->persist($uf);
        }

        $manager->flush();
    }

}