<?php

namespace App\Classes;

/**
 * Interface PlantInterface
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Classes
 */
interface PlantInterface
{
    public function getType():string;
}