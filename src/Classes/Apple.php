<?php

namespace App\Classes;

/**
 * Class Apple
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Classes
 */
class Apple extends Fruit
{
    protected $type = 'Apple-Fruit';
}