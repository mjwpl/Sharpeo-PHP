<?php

namespace App\Classes;

/**
 * Class Orange
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Classes
 */
class Orange extends Fruit
{
    protected $type = 'Orange-Fruit';

    static function make()
    {
        return 'Orange juice';
    }
}