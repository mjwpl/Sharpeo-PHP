<?php

namespace App\Classes;

/**
 * Class Vegetable
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Classes
 */
abstract class Vegetable extends Plant
{

}