<?php

namespace App\Classes;

/**
 * Class Plant
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Classes
 */
class Plant implements PlantInterface
{
    protected $type = 'Other';

    public function getType():string
    {
        return $this->type;
    }
}