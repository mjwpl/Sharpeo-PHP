<?php

namespace App\Classes;

/**
 * Class Cabbage
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Classes
 */
class Cabbage extends Vegetable
{
    protected $type = 'Cabbage-Vegetable';
}