<?php

namespace App\Classes;

/**
 * Class Potato
 *
 * @author Mariusz Jakubowski <mariusz@mjw.pl>
 * @copyright 2018 Mariusz Jakubowski, Sharpeo, Sharpeo's client
 * @package App\Classes
 */
class Potato extends Vegetable
{
    protected $type = 'Potato-Vegetable';
}